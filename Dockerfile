FROM python:3.10.6-slim

# Set defaults environment variables
ENV APP_HOST="0.0.0.0"
ENV APP_PORT="5002"
ENV DYNAMO_DB_END_POINT_URL="http://127.0.0.1:8002"
ENV DYNAMO_DB_TABLE_NAME="devops-challenge"
ENV HOME_APP=/home/appuser
ENV PATH="${HOME_APP}/.local/bin:$PATH"

# Create user and home dir
RUN useradd --create-home appuser

# Use virtual env.
COPY Pipfile .
COPY Pipfile.lock .
RUN chown appuser:appuser ./Pipfile
RUN chown appuser:appuser ./Pipfile.lock
USER appuser
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install --upgrade pipenv
RUN pip install pipenv
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy --system

# Copy app files to docker.
COPY ./app ${HOME_APP}

USER root
WORKDIR ${HOME_APP}
RUN chown -R appuser:appuser ${HOME_APP}

USER appuser
WORKDIR ${HOME_APP}

EXPOSE ${APP_PORT}
CMD ["/bin/sh", "-c", "python3 --version && pip3 freeze &&  python3 app.py"]