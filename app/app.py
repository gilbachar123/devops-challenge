import os
from  os import path 
import logging.config
import boto3
from flask import Flask, jsonify
from waitress import serve

### Init application. ###
application = Flask(__name__)

### Logger. ###
def init_logger():
    log_config_path = path.join(path.dirname(path.abspath(__file__)), 'logging.ini')
    logging.config.fileConfig(log_config_path, disable_existing_loggers=False)

### Router. ###
@application.route('/health', methods=['GET'])
def get_health():
    logging.info('GET: /health')
    container_url = os.environ.get('CONTAINER_URL')
    return jsonify({"status": "Healthy!","container": container_url}), 200

@application.route('/secret', methods=['GET'])
def get_secret():
    # Get dynamodb parameters.
    dynamodb_endpoint_url = os.environ.get('DYNAMO_DB_END_POINT_URL')
    dynamodb_table_name = os.environ.get('DYNAMO_DB_TABLE_NAME')

    # Get item from table.
    dynamodb = boto3.resource('dynamodb', endpoint_url = dynamodb_endpoint_url)
    table = dynamodb.Table(dynamodb_table_name)
    response = table.get_item( Key = { 'codeName': 'theDoctor' })

    # Parse response.
    code_name = response['Item']['codeName']
    secret_code = response['Item']['secretCode']
    logging.info('GET: /secret')
    return jsonify({"codeName": f"{code_name}","secretCode": f"{secret_code}"}), 200


#### Init logger and start application. ####
if __name__ == '__main__':
    init_logger()
    app_host = os.environ.get('APP_HOST')
    app_port = os.environ.get('APP_PORT')
    logging.info(f'Starting application: app_host={app_host} app_port={app_port}')
    serve(application, host=app_host, port=int(app_port))