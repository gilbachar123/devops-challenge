#!/bin/bash
set -x
APP_HOST=0.0.0.0
APP_PORT=5000
DYNAMO_DB_END_POINT_URL=http://127.0.0.1:8000

# Run Project.
docker-compose --env-file ../.env_docker_compose.example up -d
sleep 5

# List db tabels and query results.
aws dynamodb scan --table-name devops-challenge --endpoint-url ${DYNAMO_DB_END_POINT_URL}
curl ${APP_HOST}:${APP_PORT}/health
curl ${APP_HOST}:${APP_PORT}/secret

# Destroy. 
docker-compose down

